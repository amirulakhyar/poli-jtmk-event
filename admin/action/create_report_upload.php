<?php
session_start();

if (!isset($_SESSION['jtmk-admin'])) {
  $_SESSION['msg'] = "You must log in first";
  header('location: login.php');
}

if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['jtmk-admin']);
  header("location: login.php");
}
$event_name=$_POST['name'];
$username=$_POST['username'];
$orgDate=$_POST['date'];
$date = date("d-m-Y", strtotime($orgDate));
$place=$_POST['place'];
$organize=$_POST['organize'];
date_default_timezone_set("Asia/Kuala_Lumpur");
$rand=date("dmYHis").rand ( 0 , 10 );
include('../config/db_config.php');
$money ="report_money_".$rand;
$image="report_image_".$rand;
$kehadiran="report_kehadiran_".$rand;

  $sql1 = "INSERT INTO `report`(`username`,`report_type`,`report_page_2_nama_program`, `report_page_2_penganjur`,`report_page2_tarikh_sebernar`, `report_page_2_tempat_sebernar`,`report_kewangan`,`report_gambar`,`report_kehadiaran`)
  VALUES ('$username','1','$event_name','$organize','$date','$place', '$money' , '$image' , '$kehadiran')";
  if ($db->query($sql1) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql1 . "<br>" . $db->error;
}
  $sql2 = "
  CREATE TABLE report_image_$rand (
  `report_image_id` INT NOT NULL AUTO_INCREMENT ,
  `report_image_image` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`report_image_id`)) ENGINE = InnoDB";
  if ($db->query($sql2) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql2 . "<br>" . $db->error;
};
  $sql3 = "
  CREATE TABLE report_kehadiran_$rand (
  `report_kehadiran_id` INT NOT NULL AUTO_INCREMENT ,
  `report_kehadiran_name` VARCHAR(100) NOT NULL ,
  `report_kehadiran_jumlah` INT(11) NOT NULL ,
  `report_kehadiran_hadir` INT(11) NOT NULL ,
  `report_kehadiran_tidak_hadir` INT(11) NOT NULL ,
  PRIMARY KEY (`report_kehadiran_id`)) ENGINE = InnoDB
";
if ($db->query($sql3) === TRUE) {
  echo "New record created successfully";
} else {
  echo "Error: " . $sql3 . "<br>" . $db->error;
}
  $sql4="
  CREATE TABLE report_money_$rand (
  `report_money` INT NOT NULL AUTO_INCREMENT ,
  `report_money_name` VARCHAR(100) NOT NULL ,
  `	report_money_unit` INT(11) NOT NULL ,
  `report_money_price` DOUBLE(11,2) NOT NULL ,
  PRIMARY KEY (`report_money`)) ENGINE = InnoDB
  ";
  if ($db->query($sql4) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql4 . "<br>" . $db->error;
}



 header("location:../report.php");

 ?>
