<?php
session_start();

if (!isset($_SESSION['jtmk-admin'])) {
  $_SESSION['msg'] = "You must log in first";
  header('location: login.php');
}

if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['jtmk-admin']);
  header("location: login.php");
}
include('../config/db_config.php');
$id=$_POST['idr'];
$report_page_1_1 = $_POST['report_page_1_1'];
$report_page_1_2 = $_POST['report_page_1_2'];
$report_page_1_3 = $_POST['report_page_1_3'];
$report_page_1_4 = $_POST['report_page_1_4'];
$report_page_1_5 = $_POST['report_page_1_5'];
$report_page_1_6 = $_POST['report_page_1_6'];
$report_page_1_7 = $_POST['report_page_1_7'];
$report_page_1_8 = $_POST['report_page_1_8'];
$report_page_2_nama_program=$_POST['report_page_2_nama_program'];
$report_page_2_penganjur=$_POST['report_page_2_penganjur'];
$report_page_2_kerjasama=$_POST['report_page_2_kerjasama'];
$report_page2_tarikh_asal=$_POST['report_page2_tarikh_asal'];
$report_page2_tarikh_sebernar=$_POST['report_page2_tarikh_sebernar'];
$report_page_2_tempat_asal=$_POST['report_page_2_tempat_asal'];
$report_page_2_tempat_sebernar=$_POST['report_page_2_tempat_sebernar'];
$report_page_2_jumlah_peserta_anggaran=$_POST['report_page_2_jumlah_peserta_anggaran'];
$report_page_2_jumlah_peserta_sebernar=$_POST['report_page_2_jumlah_peserta_sebernar'];
$report_page_2_perasmi_perasmlan=$_POST['report_page_2_perasmi_perasmlan'];
$report_page_2_penutup=$_POST['report_page_2_penutup'];
$report_page_2_jumlah_pendapatan_sebenar=$_POST['report_page_2_jumlah_pendapatan_sebenar'];
$report_page_2_jumlah_perbelanjaan_sebenar=$_POST['report_page_2_jumlah_perbelanjaan_sebenar'];
$report_page_3_pengisian=$_POST['report_page_3_pengisian'];
$report_page_3_kesimpulan=$_POST['report_page_3_kesimpulan'];
$report_page_3_penutup=$_POST['report_page_3_penutup'];


$sql =
"
UPDATE `report` SET
`report_page_1_1`='$report_page_1_1',
`report_page_1_2`='$report_page_1_2',
`report_page_1_3`='$report_page_1_3',
`report_page_1_4`='$report_page_1_4',
`report_page_1_5`='$report_page_1_5',
`report_page_1_6`='$report_page_1_6',
`report_page_1_7`='$report_page_1_7',
`report_page_1_8`='$report_page_1_8',
`report_page_2_nama_program`='$report_page_2_nama_program',
`report_page_2_penganjur`='$report_page_2_penganjur',
`report_page_2_kerjasama`='$report_page_2_kerjasama',
`report_page2_tarikh_asal`='$report_page2_tarikh_asal',
`report_page2_tarikh_sebernar`='$report_page2_tarikh_sebernar',
`report_page_2_tempat_asal`='$report_page_2_tempat_asal',
`report_page_2_tempat_sebernar`='$report_page_2_tempat_sebernar',
`report_page_2_jumlah_peserta_anggaran`='$report_page_2_jumlah_peserta_anggaran',
`report_page_2_jumlah_peserta_sebernar`='$report_page_2_jumlah_peserta_sebernar',
`report_page_2_perasmi_perasmlan`='$report_page_2_perasmi_perasmlan',
`report_page_2_penutup`='$report_page_2_penutup',
`report_page_2_jumlah_pendapatan_sebenar`='$report_page_2_jumlah_pendapatan_sebenar',
`report_page_2_jumlah_perbelanjaan_sebenar`='$report_page_2_jumlah_perbelanjaan_sebenar',
`report_page_3_pengisian`='$report_page_3_pengisian',
`report_page_3_kesimpulan`='$report_page_3_kesimpulan',
`report_page_3_penutup`='$report_page_3_penutup'

 WHERE `report_id`='$id'
";
if ($db->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $db->error;
}
header("location:../report_edit.php?id=".$id);
 ?>
