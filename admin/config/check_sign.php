<?php
	session_start();

	if (!isset($_SESSION['jtmk-admin'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}

	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['jtmk-admin']);
		header("location: login.php");
	}
$username = $_SESSION['jtmk-admin'];
  include('config/db_config.php');
?>
