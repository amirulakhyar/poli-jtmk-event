<?php
$data = 'wow';
require("plugins/pdf/fpdf.php");
$pdf = new FPDF('p', 'mm', 'A4');
$pdf->SetAutoPageBreak(TRUE,15);
include('config/db_config.php');
$id=$_GET['id'];
$sql = "SELECT * FROM `report` WHERE `report_id`='$id'";
$result = $db->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      	$report_page_1_1=$row['report_page_1_1'];
        $report_page_1_2=$row['report_page_1_2'];
        $report_page_1_3=$row['report_page_1_3'];
        $report_page_1_4=$row['report_page_1_4'];
        $report_page_1_5=$row['report_page_1_5'];
        $report_page_1_6=$row['report_page_1_6'];
        $report_page_1_7=$row['report_page_1_7'];
        $report_page_1_8=$row['report_page_1_8'];
        $report_page_2_nama_program=$row['report_page_2_nama_program'];
        $report_page_2_penganjur=$row['report_page_2_penganjur'];
        $report_page_2_kerjasama=$row['report_page_2_kerjasama'];
        $report_page2_tarikh_asal=$row['report_page2_tarikh_asal'];
      	$report_page2_tarikh_sebernar=$row['report_page2_tarikh_sebernar'];
        $report_page_2_tempat_asal=$row['report_page_2_tempat_asal'];
        $report_page_2_tempat_sebernar=$row['report_page_2_tempat_sebernar'];
        $report_page_2_jumlah_peserta_anggaran=$row['report_page_2_jumlah_peserta_anggaran'];
        $report_page_2_jumlah_peserta_sebernar=$row['report_page_2_jumlah_peserta_sebernar'];
        $report_page_2_perasmi_perasmlan=$row['report_page_2_perasmi_perasmlan'];
        $report_page_2_penutup=$row['report_page_2_penutup'];
        $report_page_2_jumlah_pendapatan_sebenar=$row['report_page_2_jumlah_pendapatan_sebenar'];
        $report_page_2_jumlah_perbelanjaan_sebenar=$row['report_page_2_jumlah_perbelanjaan_sebenar'];
        $report_page_3_pengisian=$row['report_page_3_pengisian'];
        $report_page_3_kesimpulan=$row['report_page_3_kesimpulan'];
        $report_page_3_penutup=$row['report_page_3_penutup'];
        $report_page_3_disediakan=$row['report_page_3_disediakan'];
        $report_page_3_disemak=$row['report_page_3_disemak'];
        $report_kewangan=$row['report_kewangan'];
        $report_kewangan_sumber=$row['report_kewangan_sumber'];
        $report_gambar=$row['report_gambar'];
        $report_kehadiaran=$row['report_kehadiaran'];

    }
}


// PAGE 1
$pdf->AddPage();
$pdf->Image('dist/img/logo_p.png',70,40,70);
$pdf->SetFont('Arial',"",14);
$pdf->Cell(130	,80,'',0,1);
$pdf->Cell(0,5,'LAPORAN',0,1,'C');
$pdf->Cell(0,5,$report_page_2_nama_program,0,1,'C');
$pdf->Cell(0,5,'',0,1);
$pdf->Cell(0,5,'TARIKH',0,1,'C');
$pdf->Cell(0,5,$report_page2_tarikh_asal,0,1,'C');
$pdf->Cell(0,5,'',0,1);
$pdf->Cell(0,5,'TEMPAT',0,1,'C');
$pdf->Cell(0,5,$report_page_2_tempat_sebernar,0,1,'C');
$pdf->Cell(0,5,'',0,1);
$pdf->Cell(0,5,'ANJURAN',0,1,'C');
$pdf->Cell(0,5,$report_page_2_penganjur,0,1,'C');
$pdf->Cell(0,30,'',0,1);
$pdf->SetFont('Times',"",12);
$pdf->Cell(15,10,'BIL',1,0,'C');
$pdf->Cell(130,10,'PERKARA',1,0,'C');
$pdf->Cell(50,10,'Tanda (/) ',1,1,'C');
$pdf->Cell(15,10,'1',1,0,'C');
$pdf->Cell(130,10,'Sila tandakan  Generic Students Attribute (GSA) ',1,0,'C');
$pdf->Cell(50,10,'',1,1,'C');
$pdf->Cell(15,10,'1.1',1,0,'C');
$pdf->Cell(130,10,'Kemahiran komunikasi',1,0,'C');
if($report_page_1_1=='1'){$pdf->Cell(50,10,'/ ',1,1,'C');}
else{$pdf->Cell(50,10,'',1,1,'C');}
$pdf->Cell(15,10,'1.2',1,0,'C');
$pdf->Cell(130,10,'Pemikiran kritis dan kemahiran  menyelesaikan masalah',1,0,'C');
if($report_page_1_2=='1'){$pdf->Cell(50,10,'/ ',1,1,'C');}
else{$pdf->Cell(50,10,'',1,1,'C');}
$pdf->Cell(15,10,'1.3',1,0,'C');
$pdf->Cell(130,10,'Kemahiran kerja berpasukan',1,0,'C');
if($report_page_1_3=='1'){$pdf->Cell(50,10,'/ ',1,1,'C');}
else{$pdf->Cell(50,10,'',1,1,'C');}
$pdf->Cell(15,10,'1.4',1,0,'C');
$pdf->Cell(130,10,'Pembelajaran berterusan dan  pengurusan maklumat',1,0,'C');
if($report_page_1_4=='1'){$pdf->Cell(50,10,'/ ',1,1,'C');}
else{$pdf->Cell(50,10,'',1,1,'C');}
$pdf->Cell(15,10,'1.5',1,0,'C');
$pdf->Cell(130,10,'Kemahiran keusahawanan',1,0,'C');
if($report_page_1_5=='1'){$pdf->Cell(50,10,'/ ',1,1,'C');}
else{$pdf->Cell(50,10,'',1,1,'C');}
$pdf->Cell(15,10,'1.6',1,0,'C');
$pdf->Cell(130,10,'Moral dan etika profesional',1,0,'C');
if($report_page_1_6=='1'){$pdf->Cell(50,10,'/ ',1,1,'C');}
else{$pdf->Cell(50,10,'',1,1,'C');}
$pdf->Cell(15,10,'1.7',1,0,'C');
$pdf->Cell(130,10,'Kemahiran kepimpinan',1,0,'C');
if($report_page_1_7=='1'){$pdf->Cell(50,10,'/ ',1,1,'C');}
else{$pdf->Cell(50,10,'',1,1,'C');}
$pdf->Cell(15,10,'2',1,0,'C');
$pdf->Cell(130,10,'Adakah program ini berbentuk Corporate Social Responsibility(CSR) ?',1,0,'C');
if($report_page_1_8=='1'){$pdf->Cell(50,10,'/ ',1,1,'C');}
else{$pdf->Cell(50,10,'',1,1,'C');}
//PAGE 2
$pdf->AddPage();
$pdf->SetFont('Arial',"B",14);
$pdf->Cell(0,20,'',0,1,'C');
$pdf->Cell(0,5,' LAPORAN AKTIVIT',0,1,'C');
$pdf->Cell(0,5,' UNIT PENGURUSAN PSIKOLOGI',0,1,'C');
$pdf->SetFont('Arial',"B",12);
$pdf->Cell(0,5,'POLITEKNIK SULTAN MIZAN ZAINAL ABIDIN, DUNGUN TERENGGANU',0,1,'C');
$pdf->Cell(0,10,'',0,1,'C');
$pdf->SetFont('Arial',"B",11);
$pdf->Cell(15,5,'1.0',0,0,'C');
$pdf->Cell(100,5,'BUTIRAN PROGRAM',0,1);
$pdf->SetFont('Arial',"",11);
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'a) Nama Program	:',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,$report_page_2_nama_program,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'b) Penganjur',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(5,5,$report_page_2_penganjur,0,0);
$pdf->Cell(70,5,'',0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'c) Dengan Kerjasama',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,$report_page_2_kerjasama,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'d) Tarikh Asal',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,$report_page2_tarikh_asal,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'e) Tarikh Sebenar',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,$report_page2_tarikh_sebernar,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'f) Tempat Asal	',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,$report_page_2_tempat_asal,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'g) Tempat Sebenar',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,$report_page_2_tempat_sebernar,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'h) Jumlah Peserta (Anggaran) 	',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,$report_page_2_jumlah_peserta_anggaran,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'i) Jumlah Peserta (Sebenar)   ',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,$report_page_2_jumlah_peserta_sebernar,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'j) Perasmi Perasmian	',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,$report_page_2_perasmi_perasmlan,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'k) Perasmi Penutup',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,$report_page_2_penutup,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'l) Jumlah Pendapatan Sebenar ',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,'RM '.$report_page_2_jumlah_pendapatan_sebenar,0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->Cell(15,5,'',0,0,'C');
$pdf->Cell(60,5,'m) Jumlah Perbelanjaan Sebenar 	',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(70,5,'RM '.$report_page_2_jumlah_perbelanjaan_sebenar,0,1);

//PAGE 3
$pdf->AddPage();
$pdf->SetFont('Arial',"B",11);
$pdf->Cell(0,20,'',0,1,'C');
$pdf->Cell(15,5,'2.0',0,0,'C');
$pdf->Cell(100,5,'PENGISIAN / PELAKSANAAN PROGRAM',0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->SetFont('Arial',"",11);
$cellWidth=180;
$cellHeight=5;
if($pdf->GetStringWidth($report_page_3_pengisian) < $cellWidth){
  $line=1;
}else{
  $textLength=strlen($report_page_3_pengisian);
  $errMargin=10;
  $startChar=0;
  $maxChar=0;
  $textArray=array();
  $tmpString="";
  while($startChar < $textLength){
    while(
    $pdf->GetStringWidth( $tmpString ) < ($cellWidth-$errMargin) &&
    ($startChar+$maxChar) < $textLength ) {
      $maxChar++;
      $tmpString=substr($report_page_3_pengisian,$startChar,$maxChar);
    }
    $startChar=$startChar+$maxChar;
    array_push($textArray,$tmpString);
    $maxChar=0;
    $tmpString='';
  }
  $line=count($textArray);
}
$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->MultiCell($cellWidth,$cellHeight,$report_page_3_pengisian,0,1);
$pdf->Cell(0,30,'',0,1,'C');
$pdf->SetFont('Arial',"B",11);
$pdf->Cell(15,5,'3.0',0,0,'C');
$pdf->Cell(100,5,'KESIMPULAN',0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->SetFont('Arial',"",11);
$cellWidth=180;
$cellHeight=5;
if($pdf->GetStringWidth($report_page_3_kesimpulan) < $cellWidth){
  $line=1;
}else{
  $textLength=strlen($report_page_3_kesimpulan);
  $errMargin=10;
  $startChar=0;
  $maxChar=0;
  $textArray=array();
  $tmpString="";
  while($startChar < $textLength){
    while(
    $pdf->GetStringWidth( $tmpString ) < ($cellWidth-$errMargin) &&
    ($startChar+$maxChar) < $textLength ) {
      $maxChar++;
      $tmpString=substr($report_page_3_kesimpulan,$startChar,$maxChar);
    }
    $startChar=$startChar+$maxChar;
    array_push($textArray,$tmpString);
    $maxChar=0;
    $tmpString='';
  }
  $line=count($textArray);
}
$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->MultiCell($cellWidth,$cellHeight,$report_page_3_kesimpulan,0,1);
$pdf->Cell(0,30,'',0,1,'C');
$pdf->SetFont('Arial',"B",11);
$pdf->Cell(15,5,'4.0',0,0,'C');
$pdf->Cell(100,5,'PENUTUP',0,1);
$pdf->Cell(0,5,'',0,1,'C');
$pdf->SetFont('Arial',"",11);
$cellWidth=180;
$cellHeight=5;
if($pdf->GetStringWidth($report_page_3_penutup) < $cellWidth){
  $line=1;
}else{
  $textLength=strlen($report_page_3_penutup);
  $errMargin=10;
  $startChar=0;
  $maxChar=0;
  $textArray=array();
  $tmpString="";
  while($startChar < $textLength){
    while(
    $pdf->GetStringWidth( $tmpString ) < ($cellWidth-$errMargin) &&
    ($startChar+$maxChar) < $textLength ) {
      $maxChar++;
      $tmpString=substr($report_page_3_penutup,$startChar,$maxChar);
    }
    $startChar=$startChar+$maxChar;
    array_push($textArray,$tmpString);
    $maxChar=0;
    $tmpString='';
  }
  $line=count($textArray);
}
$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->MultiCell($cellWidth,$cellHeight,$report_page_3_penutup,0,1);
$pdf->Cell(0,30,'',0,1,'C');
$pdf->SetFont('Arial',"B",11);
$pdf->Cell(120,5,'Disediakan Oleh',0,0);
$pdf->Cell(50,5,'Disemak Oleh',0,1);
$pdf->Cell(0,30,'',0,1);
$pdf->SetFont('Arial',"",11);
$pdf->Cell(60,5,$report_page_3_disediakan,0,0);
$pdf->Cell(60,5,'',0,0);
$pdf->Cell(50,5,$report_page_3_disemak,0,1);
// PAGE 4
$pdf->AddPage();
$pdf->SetFont('Arial',"B",11);
$pdf->Cell(0,10,'',0,1,'C');
$pdf->Cell(0,20,'LAPORAN KEWANGAN',0,1,);
$pdf->SetFont('Arial',"",11);
$pdf->Cell(30,5,'PROGRAM',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(100,5,$report_page_2_nama_program,0,1);
$pdf->Cell(30,5,'ANJURAN',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(100,5,$report_page_2_penganjur,0,1);
$pdf->Cell(30,5,'TARIKH',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(100,5,$report_page2_tarikh_sebernar,0,1);
$pdf->Cell(30,5,'TEMPAT',0,0);
$pdf->Cell(5,5,':',0,0);
$pdf->Cell(100,5,$report_page_2_tempat_sebernar,0,1);
$pdf->Cell(0,20,'',0,1,'C');
$pdf->Cell(20,5,'No',1,0,'C');
$pdf->Cell(100,5,'Details',1,0,"C");
$pdf->Cell(25,5,'Price',1,0,'C');
$pdf->Cell(20,5,'Unit',1,0,'C');
$pdf->Cell(25,5,'Total',1,1,'C');
$result = mysqli_query($db,"SELECT * FROM $report_kewangan");
$no =0;
$grand_total=0;
while ($row=mysqli_fetch_array($result))
{
  $no++;
  $pdf->Cell(20,5,$no,1,0,'C');
  $pdf->Cell(100,5,$row['report_money_name'],1,0);
  $pdf->Cell(25,5,"RM ".$row['report_money_price'],1,0,'C');
  $pdf->Cell(20,5,$row['	report_money_unit'],1,0,'C');
  $total =$row['	report_money_unit']*$row['report_money_price'];
  $grand_total=$grand_total+$total;
  $pdf->Cell(25,5,"RM ".$total,1,1,'C');
}
$pdf->Cell(20,5,'',0,0,'C');
$pdf->Cell(100,5,'',0,0);
$pdf->Cell(25,5,'',0,0,'C');
$pdf->Cell(20,5,'Total',1,0,'C');
$pdf->Cell(25,5,"RM ".$grand_total,1,1,'C');
$pdf->Cell(0,10,'',0,1,'C');
$pdf->Cell(0,5,'Sumber peruntukan kewangan ;-',0,1,);
$pdf->Cell(0,10,$report_kewangan_sumber,0,1);

// PAGE 5

$no=0;
$result = mysqli_query($db,"SELECT * FROM $report_gambar");
while ($row=mysqli_fetch_array($result))
{
$no++;
$pdf->AddPage();
$pdf->SetFont('Arial',"B",11);
$pdf->Cell(0,10,'',0,1,'C');
$pdf->Cell(0,20,'GAMBAR - GAMBAR SEPANJANG PROGRAM',0,1,'C');
$pdf->Image('upload_image/'.$row['report_image_image'],50,70,120);
$pdf->Cell(0,130,'',0,1,'C');
$pdf->Cell(0,20,'GAMBAR '.$no,0,1,'C');

}
// PAGE 6
$pdf->AddPage();
$pdf->SetFont('Arial',"B",11);
$pdf->Cell(0,20,'',0,1,'C');
$pdf->Cell(0,5,'ANALISIS KEHADIRAN PESERTA',0,1,"C");
$pdf->Cell(0,5,'',0,1,'C');
$pdf->SetFont('Arial',"",11);
$pdf->Cell(0,10,'',0,1,"C");
$pdf->Cell(10,5,'BIL',1,0,"C");
$pdf->Cell(50,5,'PESERTA',1,0,"C");
$pdf->Cell(36,5,'JUMLAH PESERTA',1,0,"C");
$pdf->Cell(30,5,'JUMLAH HADIR',1,0,"C");
$pdf->Cell(43,5,'JUMLAH TIDAK HADIR',1,0,"C");
$pdf->Cell(25,5,'PESERTA',1,1,"C");

$result = mysqli_query($db,"SELECT * FROM $report_kehadiaran");
$no =0;
$jumlah=0;
$hadir=0;
$tak_hadir=0;
while ($row=mysqli_fetch_array($result))
{
  $no++;
  $pdf->Cell(10,5,$no,1,0,"C");
  $pdf->Cell(50,5,$row['report_kehadiran_name'],1,0,"C");
  $pdf->Cell(36,5,$row['report_kehadiran_jumlah'],1,0,"C");
  $jumlah=$jumlah+$row['report_kehadiran_jumlah'];
  $pdf->Cell(30,5,$row['report_kehadiran_hadir'],1,0,"C");
  $hadir= $hadir+$row['report_kehadiran_hadir'];
  $pdf->Cell(43,5,$row['report_kehadiran_tidak_hadir'],1,0,"C");
  $tak_hadir=$tak_hadir+$row['report_kehadiran_tidak_hadir'];
  $per = ($row['report_kehadiran_hadir'] /$row['report_kehadiran_jumlah'] )*100;
  $pdf->Cell(25,5,$per."%" ,1,1,"C");
}

$pdf->Cell(10,5,'',0,0,"C");
$pdf->Cell(50,5,'TOTAL',1,0,"C");
$pdf->Cell(36,5,$jumlah,1,0,"C");
$pdf->Cell(30,5,$hadir,1,0,"C");
$pdf->Cell(43,5,$tak_hadir,1,0,"C");
if($jumlah>0)
{
  $perT = ($hadir/$jumlah)*100;
}
else {
    $perT =0;
}

$pdf->Cell(25,5,$perT."%",1,1,"C");





$pdf->OutPut();


 ?>
