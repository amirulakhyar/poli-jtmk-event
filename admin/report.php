<?php
      include('config/check_sign.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JTMK SYSTEM</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
	<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Home</a>
      </li>
    </ul>
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
  <a class="nav-link" href="logout.php">
    <i class="fas fa-sign-out-alt"></i>
    <span class="badge badge-danger navbar-badge"></span>
  </a>
</li>
    </ul>
  </nav>
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index.php" class="brand-link">
      <img src="dist/img/Logo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>JTMK SYSTEM</b></span>
    </a>
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $username ?></a>
        </div>
      </div>
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="index.php" class="nav-link">
              <i class="fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="report.php" class="nav-link">
              <i class="fas fa-file"></i>
              <p>
                Report
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="achievement.php" class="nav-link">
              <i class="fas fa-award"></i>
              <p>
                Achievement
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="calendar.php" class="nav-link">
              <i class="fas fa-calendar-alt"></i>
              <p>
                Calendar
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="user.php" class="nav-link">
              <i class="fas fa-user"></i>
              <p>
                User
              <p>
            </a>
          </li>

          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="fas fa-cog"></i>
              <p>
                Setting
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="logout.php" class="nav-link">
              <i class="fas fa-sign-out-alt"></i>
              <p>
                Log Out
              <p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Report</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">

	<div class="card">
	<div class="card-header">
		<h3 class="card-title">List Report</h3>
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
				<i class="fas fa-minus"></i></button>
		</div>
	</div>

	<div class="card-body">
		<table id="tableStyle1" class="table table-bordered table-hover">
			<thead>
			<tr>
				<th>Date</th>
				<th>Type</th>
				<th>Report Name</th>
				<th>Name</th>
				<th>Action</th>
			</tr>
			</thead>

			<tbody>
        <?php
        $sql = "SELECT * FROM `report`";
        $result = $db->query($sql);
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            echo "
            <tr>
      				<td>".$row['report_page2_tarikh_sebernar']."</td>";
              if($row['report_type']==0)
              {
                echo"	<td>UPLOAD</td>";
              }
              else
              {
                echo"	<td>GENERATE</td>";
              }
              echo"
      				<td>".$row['report_page_2_nama_program']."</td>
      				<td>".$row['username']."</td>
      				<td>";
              if($row['report_type']==0)
              {
                echo"<a target='_blank' href='upload_image/".$row['report_file_name']."'><button type='button' class='btn btn-primary'>Report</button></a> ";
              }
              else {
                  echo"<a target='_blank' href='main_report.php?id=".$row['report_id']."'><button type='button' class='btn btn-primary'>Report</button></a> ";
              }


              if($row['report_type']==0)
              {
                echo"";
              }
              else {
                  echo" <a href='report_edit.php?id=".$row['report_id']."'><button type='button' class='btn btn-warning'>Edit</button></a>";
              }
              echo"
              <a href='action/delete_report.php?id=".$row['report_id']."'><button type='button' class='btn btn-danger'>Delete</button></a>
              </td>
      			</tr>
            ";

        }
        }
         ?>
			<tr>

			</tr>

		</tbody>
		</table>
	</div>
	</div>
<div class="row">
<div class="col-md-6">
	<div class="card card-primary">
		<div class="card-header">
			<h3 class="card-title">Upload Report</h3>
		</div>
		<form action="action/create_report.php" method="POST" autocomplete="off" enctype="multipart/form-data">
			<div class="card-body">
				<div class="form-group">
					<label for="exampleInputEmail1">Report Name</label>
					<input type="text" name="name" class="form-control" required>
          	<input type="text" name="username" value="<?php echo $username;?>" style="display:none"class="form-control">
				</div>
				<div class="form-group">
					<label for="exampleInputFile">Upload Report</label>
					<div class="input-group">
						<div class="custom-file">
							<input type="file" name="file"  class="custom-file-input" id="exampleInputFile" required>
							<label class="custom-file-label" for="exampleInputFile">Choose file</label>
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
	</div>
</div>
<div class="col-md-6">
	<div class="card card-success">
		<div class="card-header">
			<h3 class="card-title">Create Report</h3>
		</div>
		<form action="action/create_report_upload.php" method="POST" autocomplete="off" >
			<div class="card-body">
				<div class="form-group">
					<label for="exampleInputEmail1">Report Name</label>
					<input type="text" class="form-control" name="name" required>
          <input type="text" name="username" value="<?php echo $username;?>" style="display:none"class="form-control" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Date</label>
					<input type="date" class="form-control" name="date" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Place</label>
					<input type="text" class="form-control" name="place" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Organize</label>
					<input type="text" class="form-control" name="organize" required>
				</div>
			</div>
			<div class="card-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
	</div>
</div>
</div>

  </section>
  </div>
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="#">JTMK SYSTEM</a>
  </footer>
</div>
<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

<script>
  $(function () {
    $('#tableStyle1').DataTable();
  });
	$(document).ready(function () {
	  bsCustomFileInput.init();
	});
</script>
</body>
</html>
