<?php
include('../config/check_sign.php');
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JTMK SYSTEM</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Home</a>
      </li>
    </ul>
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge"></span>
        </a>
      </li>
    </ul>
  </nav>
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index.php" class="brand-link">
      <img src="../dist/img/Logo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>JTMK SYSTEM</b></span>
    </a>
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $username ?></a>
        </div>
      </div>
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="../index.php" class="nav-link">
              <i class="fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../report.php" class="nav-link">
              <i class="fas fa-file"></i>
              <p>
                Report
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../student.php" class="nav-link">
              <i class="fas fa-user"></i>
              <p>
                Student
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../lecture.php" class="nav-link">
              <i class="fas fa-university"></i>
              <p>
                Lecture
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../setting.php" class="nav-link">
              <i class="fas fa-cog"></i>
              <p>
                Setting
              <p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="row">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">Laporan</h3>
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Laporan</a></li>
                  <li class="nav-item"><a class="nav-link " href="#tab_2" data-toggle="tab">Butiran</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab">Pengisian</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_4" data-toggle="tab">Laporan Kewangan</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_5" data-toggle="tab">Gambar</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_6" data-toggle="tab">Kehadiran</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_7" data-toggle="tab">Penilaian</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    <span>Sila tandakan Generic Students’ Attribute (GSA) yang terlibat dalam program yang dilaksanakan:</span> <br><br>
                    <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="customCheckbox1" value="1">
                          <label for="customCheckbox1" class="custom-control-label">Kemahiran komunikasi</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="customCheckbox2" value="1">
                          <label for="customCheckbox2" class="custom-control-label">Pemikiran kritis dan kemahiran menyelesaikan masalah</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="customCheckbox3" value="1">
                          <label for="customCheckbox3" class="custom-control-label">Kemahiran kerja berpasukan</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="customCheckbox4" value="1">
                          <label for="customCheckbox4" class="custom-control-label">Pembelajaran berterusan dan pengurusan maklumat</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="customCheckbox5" value="1">
                          <label for="customCheckbox5" class="custom-control-label">Kemahiran keusahawanan</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="customCheckbox6" value="1">
                          <label for="customCheckbox6" class="custom-control-label">Moral dan etika profesional</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="customCheckbox7" value="1">
                          <label for="customCheckbox7" class="custom-control-label">Kemahiran kepimpinan</label>
                    </div>
                    <br>
                    <span>Adakah program ini berbentuk `Corporate Social Responsibility’ (CSR) ?</span>
                    <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="customCheckbox8" value="1">
                          <label for="customCheckbox8" class="custom-control-label">Ya</label>
                    </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                    <div class="form-group">
            					<label for="exampleInputEmail1">Nama Program</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Penganjur</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Dengan Kerjasama</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Tarikh Asal</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Tarikh Sebenar</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Tempat Asal</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Tempat Sebenar</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Jumlah Peserta (Anggaran)</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Jumlah Peserta (Sebenar)</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Perasmi Perasmian</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Perasmi Penutup</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Jumlah Pendapatan Sebenar</label>
            					<input type="text" class="form-control">
            				</div>
                    <div class="form-group">
            					<label for="exampleInputEmail1">Jumlah Perbelanjaan Sebenar</label>
            					<input type="text" class="form-control">
            				</div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                    <div class="form-group">
                      <label for="exampleInputEmail1">PENGISIAN / PELAKSANAAN PROGRAM</label>
                      <textarea class="textarea" style="width: 100%; height: 300px!important; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">KESIMPULAN</label>
                      <textarea class="textarea" style="width: 100%; height: 300px!important; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">PENUTUP</label>
                      <textarea class="textarea" style="width: 100%; height: 300px!important; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                  </div>
                  <div class="tab-pane" id="tab_4">
                    <table id="tableStyle1" class="table table-bordered table-hover">
                			<thead>
                			<tr>
                				<th>Date</th>
                				<th>Type</th>
                				<th>Report Name</th>
                				<th>Name</th>
                				<th>Action</th>
                			</tr>
                			</thead>

                			<tbody>
                			<tr>
                        <td>Date</td>
                				<td>Type</td>
                				<td>Report Name</td>
                				<td>Name</td>
                				<td>Action</td>
                			</tr>

                		</tbody>
                		</table>
                  </div>
                  <div class="tab-pane" id="tab_5">
                    <table id="tableStyle2" class="table table-bordered table-hover">
                			<thead>
                			<tr>
                				<th>Date</th>
                				<th>Type</th>
                				<th>Report Name</th>
                				<th>Name</th>
                				<th>Action</th>
                			</tr>
                			</thead>

                			<tbody>
                			<tr>
                        <td>Date</td>
                				<td>Type</td>
                				<td>Report Name</td>
                				<td>Name</td>
                				<td>Action</td>
                			</tr>

                		</tbody>
                		</table>

                  </div>
                  <div class="tab-pane" id="tab_6">
                    <table id="tableStyle3" class="table table-bordered table-hover">
                			<thead>
                			<tr>
                				<th>Date</th>
                				<th>Type</th>
                				<th>Report Name</th>
                				<th>Name</th>
                				<th>Action</th>
                			</tr>
                			</thead>

                			<tbody>
                			<tr>
                        <td>Date</td>
                				<td>Type</td>
                				<td>Report Name</td>
                				<td>Name</td>
                				<td>Action</td>
                			</tr>

                		</tbody>
                		</table>
                  </div>
                  <div class="tab-pane" id="tab_7">
                    <table id="" class="table">
                			<thead>
                			<tr>
                        <th class="w-10">No</th>
                				<th>Iteam</th>
                				<th><center>1</center></th>
                				<th><center>2</center></th>
                				<th><center>3</center></th>
                				<th><center>4</center></th>
                			</tr>
                			</thead>

                			<tbody>
                			<tr>
                        <td>1</td>
                        <td>Objektif program tercapai</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <td>2</td>
                        <td>Mendapat kerjasama yang baik</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <td>3</td>
                        <td>Sasaran yang sesuai</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <td></td>
                        <td>Penilaian Perlaksanaan Program</td>
                				<td></td>
                				<td></td>
                				<td></td>
                				<td></td>
                			</tr>
                      <tr>
                        <td>4</td>
                        <td>Suasana tempat program yang sesuai/kondusif</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <td>5</td>
                        <td>Perancangan dan pelaksanaan program telah dibuat dengan lancar</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <td>6</td>
                        <td>Masa yang diperuntukkan bersesuaian</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <td>7</td>
                        <td>Kemudahan yang mencukupi</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <td>8</td>
                        <td>Makanan yang disediakan memuaskan</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <tr>
                          <td></td>
                          <td>Penilaian Keberkesanan Program terhadap peserta</td>
                  				<td></td>
                  				<td></td>
                  				<td></td>
                  				<td></td>
                  			</tr>
                        <tr>
                          <td>9</td>
                        <td>Meningkatkan pengetahuan/pemahaman</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <td>10</td>
                        <td>Dapat membentuk jalinan kerjasama yang erat</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <td>11</td>
                        <td>Dapat berkongsi idea/maklumat</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>
                      <tr>
                        <td>12</td>
                        <td>Pada keseluruhannya program ini berjaya dan bermanfaat</td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                				<td><input type="text" class="form-control"></td>
                			</tr>

                		</tbody>
                		</table>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
  </section>
  </div>
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="#">JTMK SYSTEM</a>
  </footer>
</div>
<script src="../plugins/jquery/jquery.min.js"></script>
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../dist/js/adminlte.min.js"></script>
<script src="../plugins/summernote/summernote-bs4.min.js"></script>
<script src="../plugins/datatables/jquery.dataTables.js"></script>
<script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
<script>
  $(function () {
    $('#tableStyle1').DataTable();
  });
	$(document).ready(function () {
	  bsCustomFileInput.init();
	});
  $(function () {
    $('#tableStyle2').DataTable();
  });
	$(document).ready(function () {
	  bsCustomFileInput.init();
	});
  $(function () {
    $('#tableStyle3').DataTable();
  });
	$(document).ready(function () {
	  bsCustomFileInput.init();
	});
  $(function () {
    $('#tableStyle4').DataTable();
  });
	$(document).ready(function () {
	  bsCustomFileInput.init();
	});
</script>
</body>
</html>
