<?php
  include('config/check_sign.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JTMK SYSTEM</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Home</a>
      </li>
    </ul>
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
  <a class="nav-link"  href="logout.php">
    <i class="fas fa-sign-out-alt"></i>
    <span class="badge badge-danger navbar-badge"></span>
  </a>
</li>
    </ul>
  </nav>
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index.php" class="brand-link">
      <img src="dist/img/Logo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>JTMK SYSTEM</b></span>
    </a>
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $username ?></a>
        </div>
      </div>
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="index.php" class="nav-link">
              <i class="fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="report.php" class="nav-link">
              <i class="fas fa-file"></i>
              <p>
                Report
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="achievement.php" class="nav-link">
              <i class="fas fa-award"></i>
              <p>
                Achievement
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="calendar.php" class="nav-link">
              <i class="fas fa-calendar-alt"></i>
              <p>
                Calendar
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="user.php" class="nav-link">
              <i class="fas fa-user"></i>
              <p>
                User
              <p>
            </a>
          </li>

          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="fas fa-cog"></i>
              <p>
                Setting
              <p>
            </a>
          </li>
          <li class="nav-item">
            <a href="logout.php" class="nav-link">
              <i class="fas fa-sign-out-alt"></i>
              <p>
                Log Out
              <p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Setting</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Setting</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">List Admin</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
      		<table id="tableStyle1" class="table table-bordered table-hover">
      			<thead>
      			<tr>
      				<th>Name</th>
      				<th>Username</th>
      				<th>Email</th>
      				<th>Action</th>
      			</tr>

          </thead>
          <tbody>
            <?php
            $sql = "SELECT * FROM `admin`";
            $result = $db->query($sql);
            if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
              echo
              "
              <tr>
        				<td>".$row['admin_name']."</td>
        				<td>".$row['admin_username']."</td>
        				<td>".$row['admin_email']."</td>
        				<td><a href='action/delete_admin.php?id=".$row['admin_id']."'><button type='button' class='btn btn-danger'>Delete</button></a></td>
        			</tr>
              ";
            }
          }
             ?>

      		</tbody>
      		</table>
      	</div>
      	</div>
        <div class="col-md-6">
        	<div class="card card-success">
        		<div class="card-header">
        			<h3 class="card-title">Create Admin</h3>
        		</div>
        		<form action="action/create_admin.php" method="POST" autocomplete="off">
        			<div class="card-body">
        				<div class="form-group">
        					<label for="exampleInputEmail1">Name</label>
        					<input type="text" class="form-control" name="name" required>
        				</div>
                <div class="form-group">
        					<label for="exampleInputEmail1">Username</label>
        					<input type="text" class="form-control" name="username" required>
        				</div>
                <div class="form-group">
        					<label for="exampleInputEmail1">Email</label>
        					<input type="email" class="form-control" name="email" required>
        				</div>
                <div class="form-group">
        					<label for="exampleInputEmail1">Password</label>
        					<input type="password" class="form-control" name="password" required>
        				</div>
              </div>
        			<div class="card-footer">
        				<button type="submit" class="btn btn-primary">Submit</button>
        			</div>
        		</form>
        </div>
      </div>
  </section>
  </div>
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="#">JTMK SYSTEM</a>
  </footer>
</div>
<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $('#tableStyle1').DataTable();
  });
	$(document).ready(function () {
	  bsCustomFileInput.init();
	});
</script>
</body>
</html>
