-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2020 at 05:35 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jtmk`
--

-- --------------------------------------------------------

--
-- Table structure for table `achievement`
--

CREATE TABLE `achievement` (
  `achievement_id` int(11) NOT NULL,
  `achievement_name` varchar(100) NOT NULL,
  `achievement_date` varchar(100) NOT NULL,
  `achievemen_organize` varchar(100) NOT NULL,
  `achievement_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `achievement`
--

INSERT INTO `achievement` (`achievement_id`, `achievement_name`, `achievement_date`, `achievemen_organize`, `achievement_image`) VALUES
(8, 'Hari Sukan Negara', '12 March 2020', 'Muhamad Hanif', 'achievement_image_120320202314253c9558a31-7723-4558-9fee-f69baca119ff.png'),
(9, 'Hari Hari ', '12 March 2020', 'Amirul Akhyar', 'achievement_image_1203202023172410thumb-1920-936378.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `achievement_pair`
--

CREATE TABLE `achievement_pair` (
  `achievement_pair_id` int(11) NOT NULL,
  `achievement_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_username` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_username`, `admin_email`, `admin_password`) VALUES
(2, 'Mohamad Amirul Akhyar', 'amirulakhyar', 'amirulakhyar97@gmail.com', 'd193af2039a8aed91305461dd8ce7a41');

-- --------------------------------------------------------

--
-- Table structure for table `lect`
--

CREATE TABLE `lect` (
  `lect_id` int(11) NOT NULL,
  `lect_name` varchar(100) NOT NULL,
  `lect_username` varchar(100) NOT NULL,
  `lect_email` varchar(100) NOT NULL,
  `lect_password` varchar(100) NOT NULL,
  `lect_lecturer_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lect`
--

INSERT INTO `lect` (`lect_id`, `lect_name`, `lect_username`, `lect_email`, `lect_password`, `lect_lecturer_id`) VALUES
(2, 'Mohamad Amirul Akhyar', 'wow', 'amirulakhyar97@gmail.com', 'b60cdfd9d3c76cd4b5104d96f81b9bf6', 'NWS17010049');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `report_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `report_type` int(1) NOT NULL,
  `report_file_name` varchar(100) NOT NULL,
  `report_page_1_1` int(1) NOT NULL,
  `report_page_1_2` int(1) NOT NULL,
  `report_page_1_3` int(1) NOT NULL,
  `report_page_1_4` int(1) NOT NULL,
  `report_page_1_5` int(1) NOT NULL,
  `report_page_1_6` int(1) NOT NULL,
  `report_page_1_7` int(1) NOT NULL,
  `report_page_1_8` int(1) NOT NULL,
  `report_page_2_nama_program` varchar(100) NOT NULL,
  `report_page_2_penganjur` varchar(100) NOT NULL,
  `report_page_2_kerjasama` varchar(100) NOT NULL,
  `report_page2_tarikh_asal` varchar(100) NOT NULL,
  `report_page2_tarikh_sebernar` varchar(100) NOT NULL,
  `report_page_2_tempat_asal` varchar(100) NOT NULL,
  `report_page_2_tempat_sebernar` varchar(100) NOT NULL,
  `report_page_2_jumlah_peserta_anggaran` varchar(100) NOT NULL,
  `report_page_2_jumlah_peserta_sebernar` varchar(100) NOT NULL,
  `report_page_2_perasmi_perasmlan` varchar(100) NOT NULL,
  `report_page_2_penutup` varchar(100) NOT NULL,
  `report_page_2_jumlah_pendapatan_sebenar` varchar(100) NOT NULL,
  `report_page_2_jumlah_perbelanjaan_sebenar` varchar(100) NOT NULL,
  `report_page_3_pengisian` text NOT NULL,
  `report_page_3_kesimpulan` text NOT NULL,
  `report_page_3_penutup` longtext NOT NULL,
  `report_page_3_disediakan` varchar(100) NOT NULL,
  `report_page_3_disediakan_alamat` text NOT NULL,
  `report_page_3_disemak` varchar(100) NOT NULL,
  `report_table` varchar(100) NOT NULL,
  `report_kewangan` varchar(100) NOT NULL,
  `report_kewangan_sumber` text NOT NULL,
  `report_gambar` varchar(100) NOT NULL,
  `report_kehadiaran` varchar(100) NOT NULL,
  `report_analisa_1_1` int(3) NOT NULL,
  `report_analisa_1_2` int(3) NOT NULL,
  `report_analisa_1_3` int(3) NOT NULL,
  `report_analisa_1_4` int(3) NOT NULL,
  `report_analisa_2_1` int(3) NOT NULL,
  `report_analisa_2_2` int(3) NOT NULL,
  `report_analisa_2_3` int(3) NOT NULL,
  `report_analisa_2_4` int(3) NOT NULL,
  `report_analisa_3_1` int(3) NOT NULL,
  `report_analisa_3_2` int(3) NOT NULL,
  `report_analisa_3_3` int(3) NOT NULL,
  `report_analisa_3_4` int(3) NOT NULL,
  `report_analisa_4_1` int(3) NOT NULL,
  `report_analisa_4_2` int(3) NOT NULL,
  `report_analisa_4_3` int(3) NOT NULL,
  `report_analisa_4_4` int(3) NOT NULL,
  `report_analisa_5_1` int(3) NOT NULL,
  `report_analisa_5_2` int(3) NOT NULL,
  `report_analisa_5_3` int(3) NOT NULL,
  `report_analisa_5_4` int(3) NOT NULL,
  `report_analisa_6_1` int(3) NOT NULL,
  `report_analisa_6_2` int(3) NOT NULL,
  `report_analisa_6_3` int(3) NOT NULL,
  `report_analisa_6_4` int(3) NOT NULL,
  `report_analisa_7_1` int(3) NOT NULL,
  `report_analisa_7_2` int(3) NOT NULL,
  `report_analisa_7_3` int(3) NOT NULL,
  `report_analisa_7_4` int(3) NOT NULL,
  `report_analisa_8_1` int(3) NOT NULL,
  `report_analisa_8_2` int(3) NOT NULL,
  `report_analisa_8_3` int(3) NOT NULL,
  `report_analisa_8_4` int(3) NOT NULL,
  `report_analisa_9_1` int(3) NOT NULL,
  `report_analisa_9_2` int(3) NOT NULL,
  `report_analisa_9_3` int(3) NOT NULL,
  `report_analisa_9_4` int(3) NOT NULL,
  `report_analisa_10_1` int(3) NOT NULL,
  `report_analisa_10_2` int(3) NOT NULL,
  `report_analisa_10_3` int(3) NOT NULL,
  `report_analisa_10_4` int(3) NOT NULL,
  `report_analisa_11_1` int(3) NOT NULL,
  `report_analisa_11_2` int(3) NOT NULL,
  `report_analisa_11_3` int(3) NOT NULL,
  `report_analisa_11_4` int(3) NOT NULL,
  `report_analisa_12_1` int(3) NOT NULL,
  `report_analisa_12_2` int(3) NOT NULL,
  `report_analisa_12_3` int(3) NOT NULL,
  `report_analisa_12_4` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`report_id`, `username`, `report_type`, `report_file_name`, `report_page_1_1`, `report_page_1_2`, `report_page_1_3`, `report_page_1_4`, `report_page_1_5`, `report_page_1_6`, `report_page_1_7`, `report_page_1_8`, `report_page_2_nama_program`, `report_page_2_penganjur`, `report_page_2_kerjasama`, `report_page2_tarikh_asal`, `report_page2_tarikh_sebernar`, `report_page_2_tempat_asal`, `report_page_2_tempat_sebernar`, `report_page_2_jumlah_peserta_anggaran`, `report_page_2_jumlah_peserta_sebernar`, `report_page_2_perasmi_perasmlan`, `report_page_2_penutup`, `report_page_2_jumlah_pendapatan_sebenar`, `report_page_2_jumlah_perbelanjaan_sebenar`, `report_page_3_pengisian`, `report_page_3_kesimpulan`, `report_page_3_penutup`, `report_page_3_disediakan`, `report_page_3_disediakan_alamat`, `report_page_3_disemak`, `report_table`, `report_kewangan`, `report_kewangan_sumber`, `report_gambar`, `report_kehadiaran`, `report_analisa_1_1`, `report_analisa_1_2`, `report_analisa_1_3`, `report_analisa_1_4`, `report_analisa_2_1`, `report_analisa_2_2`, `report_analisa_2_3`, `report_analisa_2_4`, `report_analisa_3_1`, `report_analisa_3_2`, `report_analisa_3_3`, `report_analisa_3_4`, `report_analisa_4_1`, `report_analisa_4_2`, `report_analisa_4_3`, `report_analisa_4_4`, `report_analisa_5_1`, `report_analisa_5_2`, `report_analisa_5_3`, `report_analisa_5_4`, `report_analisa_6_1`, `report_analisa_6_2`, `report_analisa_6_3`, `report_analisa_6_4`, `report_analisa_7_1`, `report_analisa_7_2`, `report_analisa_7_3`, `report_analisa_7_4`, `report_analisa_8_1`, `report_analisa_8_2`, `report_analisa_8_3`, `report_analisa_8_4`, `report_analisa_9_1`, `report_analisa_9_2`, `report_analisa_9_3`, `report_analisa_9_4`, `report_analisa_10_1`, `report_analisa_10_2`, `report_analisa_10_3`, `report_analisa_10_4`, `report_analisa_11_1`, `report_analisa_11_2`, `report_analisa_11_3`, `report_analisa_11_4`, `report_analisa_12_1`, `report_analisa_12_2`, `report_analisa_12_3`, `report_analisa_12_4`) VALUES
(3, 'amirulakhyar', 1, '', 0, 1, 1, 0, 1, 1, 1, 1, 'HARI SUKAN NEGARA', 'UNIT PENGURUSAN PSIKOLOGI', 'AHLI PRSP', '11 FEBRUARI 2020', '12 FEBRUARI 2020', 'KOMPLEKS SUKAN PSMZA', 'KOMPLEKS SUKAN NEGARA', '100', '200', 'Sr HAJI MOHD FIKRI BIN ISMAIL (PENGARAH', '-', '3470.00', '3470.00', 'Program ini merupakan satu program untuk mendedahkan kepada pelajar sekolah dan Politeknik tentang pembelajaran di luar kelas di samping menjalankan aktiviti yang bermanfaat . Melalui program ini, ahl', '3.1. Kelebihan/kebaikan program	 Berdasarkan daripada maklumbalas daripada peserta dan cikgu, program ini telah berjaya mencapai objektif. Program ini telah memberi peluang kepada peserta untuk lebih berani tampil kehadapan tanpa rasa gentar dan mereka dapat membantu diri untuk lebih bijak menguruskan sesebuah kumpulan. Pendedahan terhadap ilmu asas fizik kepada pelajar merupakan satu daripada objektif untuk program ini disamping mengadakan bengkel rekacipta pelancar roket.  3.2. Permasalahan dan cadangan penambahbaikan program	 Isu kekurangan ahli jawatan kuasa perlu dititikberatkan lagi supaya dapat menjaga setiap pergerakan peserta agar tidak ada yang terlepas pandang dan program ini memerlukan pengurusan yang rapi dari awal sehingga ke akhir program.', 'Secara Keseluruhannya Program FLYING DAYS dengan peruntukkan sebanyak RM 3470.00 kepada seramai 102 orang pelajar sekolah menengah di Kompleks Sukan PSMZA pada 11 FEBRUARI 2020 berjaya dilaksanakan dan mencapai objektif. ', '', '', '', '', 'report_money', ' I dont Know', 'report_image', 'report_kehadiran', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `report_image`
--

CREATE TABLE `report_image` (
  `report_image_id` int(11) NOT NULL,
  `report_image_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `report_image`
--

INSERT INTO `report_image` (`report_image_id`, `report_image_image`) VALUES
(2, 'Universiti_Putra_Malaysia_Full_Logo.png'),
(3, 'thumb-1920-936378.jpg'),
(4, 'c9558a31-7723-4558-9fee-f69baca119ff.png'),
(5, 'thumb-1920-936378.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `report_kehadiran`
--

CREATE TABLE `report_kehadiran` (
  `report_kehadiran_id` int(11) NOT NULL,
  `report_kehadiran_name` varchar(100) NOT NULL,
  `report_kehadiran_jumlah` int(11) NOT NULL,
  `report_kehadiran_hadir` int(11) NOT NULL,
  `report_kehadiran_tidak_hadir` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `report_kehadiran`
--

INSERT INTO `report_kehadiran` (`report_kehadiran_id`, `report_kehadiran_name`, `report_kehadiran_jumlah`, `report_kehadiran_hadir`, `report_kehadiran_tidak_hadir`) VALUES
(1, 'Ahli', 100, 10, 2),
(2, 'PERKERJA BUTIK', 20, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `report_money`
--

CREATE TABLE `report_money` (
  `report_money` int(11) NOT NULL,
  `report_money_name` varchar(100) NOT NULL,
  `report_money_unit` int(11) NOT NULL,
  `report_money_price` double(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `report_money`
--

INSERT INTO `report_money` (`report_money`, `report_money_name`, `report_money_unit`, `report_money_price`) VALUES
(1, 'dadsadasdas', 12, 22.55),
(2, 'MAKANAN TENGAHRI', 20, 11.50),
(3, 'MAKAN MALAM', 10, 1.59);

-- --------------------------------------------------------

--
-- Table structure for table `report_pair`
--

CREATE TABLE `report_pair` (
  `report_pair_id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `student_id` int(11) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `student_no` varchar(100) NOT NULL,
  `student_email` varchar(100) NOT NULL,
  `student_matrik` varchar(100) NOT NULL,
  `student_username` varchar(100) NOT NULL,
  `student_password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `student_name`, `student_no`, `student_email`, `student_matrik`, `student_username`, `student_password`) VALUES
(2, 'Mohamad Amirul akhyar', '', 'amiurlkhyar@2sd.co', 'nws 1qu232', 'woi', '3d8cf6bf0f86c00fb2031bdef989bf91');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `achievement`
--
ALTER TABLE `achievement`
  ADD PRIMARY KEY (`achievement_id`);

--
-- Indexes for table `achievement_pair`
--
ALTER TABLE `achievement_pair`
  ADD PRIMARY KEY (`achievement_pair_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `lect`
--
ALTER TABLE `lect`
  ADD PRIMARY KEY (`lect_id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `report_image`
--
ALTER TABLE `report_image`
  ADD PRIMARY KEY (`report_image_id`);

--
-- Indexes for table `report_kehadiran`
--
ALTER TABLE `report_kehadiran`
  ADD PRIMARY KEY (`report_kehadiran_id`);

--
-- Indexes for table `report_money`
--
ALTER TABLE `report_money`
  ADD PRIMARY KEY (`report_money`);

--
-- Indexes for table `report_pair`
--
ALTER TABLE `report_pair`
  ADD PRIMARY KEY (`report_pair_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `achievement`
--
ALTER TABLE `achievement`
  MODIFY `achievement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `achievement_pair`
--
ALTER TABLE `achievement_pair`
  MODIFY `achievement_pair_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lect`
--
ALTER TABLE `lect`
  MODIFY `lect_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `report_image`
--
ALTER TABLE `report_image`
  MODIFY `report_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `report_kehadiran`
--
ALTER TABLE `report_kehadiran`
  MODIFY `report_kehadiran_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `report_money`
--
ALTER TABLE `report_money`
  MODIFY `report_money` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `report_pair`
--
ALTER TABLE `report_pair`
  MODIFY `report_pair_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
